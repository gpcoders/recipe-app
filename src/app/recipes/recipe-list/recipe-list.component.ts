import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  /**
   * @ Declaring varibales
   * @type {any[]}
   */
  recipes: Recipe[] = [
    new Recipe('Gugu Test Recipe', 'testing description', 'https://gpcoders.com/wp-content/uploads/2018/02/shutterstock_393432979.jpg'),
    new Recipe('Gugu Test Recipe', 'testing description', 'https://gpcoders.com/wp-content/uploads/2018/02/shutterstock_393432979.jpg')
  ];

  /**
   * @ Constructor init while component load first time
   */
  constructor() { }

  /**
   * @ ngOnInit()
   */
  ngOnInit() {
  }

}
